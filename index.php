<?php
session_start();
?>
<!DOCTYPE html>
<!--Webdev Albert Jurasik-->
<html lang="pl">
    <head>
        <?php require_once 'common/head.php' ?>
    </head>
    <body>
        <div class="own-container">
            <header class="text-center py-5 mx-auto">
                <div id="logo" class=" text-white ">
                    <div  class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 pt-3">
                            <h1><i class="glyphicon glyphicon-console"></i><a href="#">Technik Informatyk</a></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 pb-3">
                            <h6><a href="#">Projekt szkolny</a></h6>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <div class="container-fluid mb-5">

            <div class="row mb-5 pb-3">
                <div class="col-lg-8 col-md-6 col-sm-5 corner">
                    
                </div>
                <div class="col-lg-4 col-md-6 col-sm-7 mx-0 my-0 px-0">
                    <div class='alert border-0 alert-dismissible fade show ' role='alert'>
                        <div class="float-right"><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <i class="fas fa-angle-double-left"></i> Przejście do drugiej strony</div>
                </div>
                </div>

            </div>

            <hgroup class="Slayer hidden-layer text-dark">

                <?php include("views/articles.php"); ?>
            </hgroup>
            <div class="Flayer">

                <div class="row my-5">
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center pb-5">
                        <h2>Witaj!</h2>
                        Znajdujesz się na stronie głównej szkolnego projektu.<br/>
                        Twórcami tej strony są uczniowie Technikum Informatycznego.<br/>
                        Mamy przyjemność przedstawić w tym miejscu naszą wiedzę, zgromadzoną do tej pory.<br/>

                    </div>
                </div>
                <hr style="width: 40% !important;"/>

                <nav>
                    <?php include("views/navigation.php") ?>
                </nav>
            </div>
        </div>
        <footer class="container-fluid pb-5">

            <?php include("views/footer.php"); ?>
            <div class="row mx-0 px-0">
                <div class="col-lg-12 col-md-12 col-sm-12 px-0">
                    <?=
                    isset($_SESSION['send']) ? " <div class='alert alert-success alert-dismissible fade show' role='alert'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    Wiadomość wysłana.
                </div>" : "";
                    unset($_SESSION['send']);
                    ?>
                </div>
            </div>
        </footer>


        <?php require_once 'common/end.php'; ?>
    </body>
</html>
