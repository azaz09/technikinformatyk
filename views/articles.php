
<div class="row mx-1 text-center ">
    <div class="col-lg-12 col-md-12 col-sm-12 mt-5"><h2>Egzaminy Zawodowe</h2>
    </div>
</div>
<article id="1st">
    <div class="row mt-5 mx-5">
        <div class="col-lg-8 col-md-8 col-sm-8 own-collapse py-3 mx-auto text-light">
            <div class="float-left "><i class="fa fa-hdd"></i> Egzamin - Kwalifikacja E.12</div>
            <div class="float-right"><i class="fa fa-plus"></i></div>

        </div>
    </div>
    <div class="row pt-0 mt-0 mx-5">
        <div class=" col-lg-8 col-md-8 col-sm-8 px-0 mx-auto">
            <div class="collapse-content px-2 py-2 mx-auto">
                <p>Jest to egzamin obejmujący obszar wiedzy oraz umiejętności praktyczne z montażu i eksploatacji komputerów osobistych oraz urządzeń peryferyjnych.</p>
                <p>Teoria trwa 60 minut i obejmuje 40 pytań zamkniętych z 4 możliwymi odpowiedziami (do wyboru jedna). Test praktyczny zajmuje 150 minut i składa się z zadań, które trzeba wykonać na wcześniej sprawdzonym i przygotowanym komputerze. Podstawowe zadania, to najczęściej:<br/>
                <h6 class="text-info">Hardware:</h6>wymiana kości RAM, dysku HDD, podłączenie karty sieciowej.<br/>
                <h6 class="text-success">Software:</h6>Instalacja sterowników, znajomość edytora lokalnych zasad grupy, edytora lokalnych zasad zabezpieczeń. Umiejętności posługiwania się CMD w Windowsie oraz Terminal'em w linux'ie.</p><br/>
                <p class="pb-0 mb-0">Nazwa i symbol cyfrowy zawodu Technik informatyk: <a target="blank" class="text-dark" href="http://archiwum.cke.edu.pl/images/stories/0000000000000002012_informatory/informator_z125_351203_ti_popr.pdf">(351203)</a><br/>
                    Przykładowe zadania: <a target="blank" class="text-dark" href="https://cke.gov.pl/images/_EGZAMIN_ZAWODOWY/informatory/zmiany/e_12_informator_errata.pdf"><i class="far fa-file-alt"></i></a></p>
            </div>
        </div>
    </div>
</article>

<article id="2nd">
    <div class="row mt-5 mx-5">
        <div class="col-lg-8 col-md-8 col-sm-8 own-collapse py-3 mx-auto text-light">
            <div class="float-left "><i class="fas fa-globe"></i> Egzamin - Kwalifikacja E.13</div>
            <div class="float-right"><i class="fa fa-plus"></i></div>

        </div>
    </div>
    <div class="row pt-0 mt-0 mx-5">
        <div class=" col-lg-8 col-md-8 col-sm-8 px-0 mx-auto">
            <div class="change-content collapse-content px-2 py-2 mx-auto">
                <p>Ten egzamin obejmuje wiedzę oraz umiejętności praktyczne z projektowania lokalnych sieci komputerowych i administrowania sieciami.</p>
                <p>Teoria trwa 60 minut i obejmuje 40 pytań zamkniętych z 4 możliwymi odpowiedziami (do wyboru jedna). Test praktyczny zajmuje 150 minut. Zadania trzeba wykonać z użyciem przygotowanych wcześniej dwóch komputerów (w tym jeden z windows server). Najczęściej można się spotkać z:  
                <h6 class="text-info">Hardware:</h6>Wykonaniem skrętki UTP według normy TIA/EIA-568-A lub TIA/EIA-568-B. Podłączenie stacji roboczej i serwera do switcha.<br/>
                <h6 class="text-success">Software:</h6>Stworzenie i dołączenie użytkowników do grup. Konfiguracja protokołu TCP/IP Konfiguracja usługi DNS i DHCP.</p><br/>
                <p class="pb-0 mb-0">Nazwa i symbol cyfrowy zawodu Technik informatyk: <a target="blank" class="text-dark" href="http://archiwum.cke.edu.pl/images/stories/0000000000000002012_informatory/informator_z125_351203_ti_popr.pdf">(351203)</a><br/>
                    Przykładowe zadania: <a target="blank" class="text-dark" href="https://cke.gov.pl/images/_EGZAMIN_ZAWODOWY/informatory/zmiany/e_13_informator_errata.pdf"><i class="far fa-file-alt"></i></a></p>
            </div>
        </div>
    </div>
</article>

<article id="3th">
    <div class="row mt-5 mx-5">
        <div class="col-lg-8 col-md-8 col-sm-8 own-collapse py-3 mx-auto text-light">
            <div class="float-left "><i class="fas fa-file-code"></i> Egzamin - Kwalifikacja E.14</div>
            <div class="float-right"><i class="fa fa-plus"></i></div>

        </div>
    </div>
    <div class="row pt-0 mt-0 mx-5">
        <div class=" col-lg-8 col-md-8 col-sm-8 px-0 mx-auto">
            <div class="change-content collapse-content px-2 py-2 mx-auto">
                <p>Ostatni z trzech egzaminów potwierdzający kwalifikacje w zawodzie. Aby go zdać należy posiadać wiedzę oraz umiejętności w zakresie: tworzenia aplikacji internetowych i baz danych oraz
                    administrowania bazami.</p>
                <p>Teoria trwa 60 minut i obejmuje 40 pytań zamkniętych z 4 możliwymi odpowiedziami (do wyboru jedna). Test praktyczny zajmuje 150 minut i składa się z zadań, które trzeba wykonać na wcześniej sprawdzonym i przygotowanym komputerze z użyciem notatnika. Najczęściej można się spotkać z:  
                <h6 class="text-warning">Teoria:</h6>
                Pytania z SQL. Pojęcia z zakresu języków: HTML, CSS, PHP, JS.<br/>
                <h6 class="text-success">Praktyka:</h6>
                Stworzyć bazę danych, wypełnić rekordy oraz utworzyć relacje. Tworzenie formularzu do dodawania zgłoszeń, raportu, oraz projektu przedstawiającego zgłoszenie. Eksportowanie bazy danych. Tworzenie grafiki wektorowej i rastrowej oraz obrazów png. Tworzenie prostej strony z użyciem HTML i CSS oraz PHP i JS.</p><br/>
                <p class="pb-0 mb-0">
                    Nazwa i symbol cyfrowy zawodu Technik informatyk: <a target="blank" class="text-dark" href="http://archiwum.cke.edu.pl/images/stories/0000000000000002012_informatory/informator_z125_351203_ti_popr.pdf">(351203)</a><br/>
                    Przykładowe zadania: <a target="blank" class="text-dark" href="https://cke.gov.pl/images/_EGZAMIN_ZAWODOWY/informatory/zmiany/e_14_informator_errata.pdf"><i class="far fa-file-alt"></i></a></p>
            </div>
        </div>
    </div>
</article>
<section class="row my-5 py-5 text-center">
    <div class="col-lg-12 col-md-12 col-sm-12 mt-5 p-2">
        <h3>Informacje dodatkowe</h3>
    </div>
</section>
<article id="aboutExams">
    <div class="row mb-5 mt-5 mx-5 px-5">
        <div clas="col-lg-8 col-md-8 col-sm-8 py-3 px-5 mx-auto">
            <h5>Kwalifikacja w zawodzie</h5>
            <p>Aby zdobyć kwalifikacje w zawodzie Technik Informatyk, trzeba zdać wszystkie egzaminy kwalifikacyjne. Są to EE.08 i EE.09 (wcześniej E.12, E.13 i E.14). Egzaminy sprawdzają wiedzę oraz umiejętności z danego zakresu informatyki. Tworzeniem, przeprowadzaniem egzaminów i wydawaniem potwierdzenia zdania kwalifikacji w zawodzie zajmuje się Centralna Komisja Egzaminacyjna.</p>
            <hr/>
        </div>
    </div>
</article>
<article id="aboutExams-2">
    <div class="row mb-5 mt-5 mx-5 px-5">
        <div clas="col-lg-8 col-md-8 col-sm-8 py-3 px-5 mx-auto">
            <h5>Zmiana egzaminów i kwalifikacji</h5>
                <p>W 2012 roku zostały wprowadzone kwalifikacje w poszczególnych zawodach na poziomie technikum.
                    Dla Technikum Informatycznego byly to przedstawione już: <h6 class="text-danger d-inline">E.12</h6>, <h6 class="text-warning d-inline">E.13</h6>, <h6 class="text-success d-inline">E.14</h6>. Od 2017 roku komisja CKE przygotowała nowy program egzaminów. Trzy kwalifikacje zamieniły się w dwie. Tak więc zamiast: <h6 class="text-danger d-inline">Montażu i eksploatacji komputerów osobistych oraz urządzeń peryferyjnych</h6> i części z <h6 class="text-warning d-inline">projektowania lokalnych sieci komputerowych</h6> otrzymaliśmy EE.08.<br/>
                A zamiast <h6 class="text-success d-inline">Tworzenia aplikacji internetowych i administrowania bazami</h6> i fragmentu z <h6 class="text-warning d-inline">administrowania sieciami</h6> mamy EE.09.
                </p>
                <p>
                    Dotąd nikt nie wie jeszcze, jak wyglądać będą egzaminy z nowych kwalifikacji. Informacja jednak pojawi się na stronie, gdy tylko uczniowie napiszą pierwsze z nich.
                </p>
            <hr/>
        </div>
    </div>
</article>

