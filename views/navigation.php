<div class="row my-3 text-center">
    <figure class="col-lg-3 col-md-6 col-sm-12 mx-auto">
        <div class="col-lg-12 col-md-12 col-sm-12 nav-to px-0">
            <a href="#" target="_blank">
                <img src="img/main.jpg" alt="mainPage" class="image"/>
                <div class="cover my-auto py-auto">
                    <div class="cover-child">
                        <div>
                            <i class="fas fa-paper-plane fa-2x "></i>
                        </div>
                        <div>
                            <span>Strona Główna</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </figure>
    <figure class="col-lg-3 col-md-6 col-sm-12 mx-auto">
        <div class="col-lg-12 col-md-12 col-sm-12 nav-to px-0">
            <a href="#" target="_blank">
                <img src="img/wlan.jpg" alt="wirelessNetwork" class="image"/>
                <div class="cover my-auto py-auto  ">
                    <div class="cover-child">
                        <div>
                            <i class="fas fa-paper-plane fa-2x "></i>
                        </div>
                        <div>
                            <span>Sieć Bezprzewodowa</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </figure>
    <figure class="col-lg-3 col-md-6 col-sm-12 mx-auto">
        <div class="col-lg-12 col-md-12 col-sm-12 nav-to px-0">
            <a href="#" target="_blank">
                <img src="img/wire.jpg" alt="wireNetwork" class="image"/>
                <div class="cover my-auto py-auto">
                    <div class="cover-child">
                        <div>
                            <i class="fas fa-paper-plane fa-2x"></i>
                        </div>
                        <div>
                            <span>Sieć Przewodowa</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </figure>
    <figure class="col-lg-3 col-md-6 col-sm-12 mx-auto">
        <div class="col-lg-12 col-md-12 col-sm-12 nav-to px-0">
            <a href="#" target="_blank">
                <img src="img/peripheral.jpg" alt="e.13" class="image"/>
                <div class="cover my-auto py-auto  ">
                    <div class="cover-child">
                        <div>
                            <i class="fas fa-paper-plane fa-2x "></i>
                        </div>
                        <div>
                            <span>Urządzenia Peryferyjne</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </figure>
</div>
<div class="row my-3 text-center">
    <figure class="col-lg-3 col-md-6 col-sm-12 mx-auto">
        <div class="col-lg-12 col-md-12 col-sm-12 nav-to px-0">
            <a href="#" target="_blank">
                <img src="img/koszt.jpg" alt="Excel" class="image"/>
                <div class="cover my-auto py-auto  ">
                    <div class="cover-child">
                        <div>
                            <i class="fas fa-paper-plane fa-2x "></i>
                        </div>
                        <div>
                            <span>Tworzenie Kosztorysów</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </figure>
    <figure class="col-lg-3 col-md-6 col-sm-12 mx-auto">
        <div class="col-lg-12 col-md-12 col-sm-12 nav-to px-0">
            <a href="#" target="_blank">
                <img src="img/winc.png" alt="komendyWindows" class="image"/>
                <div class="cover my-auto py-auto">
                    <div class="cover-child">
                        <div>
                            <i class="fas fa-paper-plane fa-2x "></i>
                        </div>
                        <div>
                            <span>Komendy Windows</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </figure>
    <figure class="col-lg-3 col-md-6 col-sm-12 mx-auto">
        <div class="col-lg-12 col-md-12 col-sm-12 nav-to px-0">
            <a href="#" target="_blank">
                <img src="img/link.jpg" alt="komendyLinux" class="image"/>
                <div class="cover my-auto py-auto  ">
                    <div class="cover-child">
                        <div>
                            <i class="fas fa-paper-plane fa-2x "></i>
                        </div>
                        <div>
                            <span>Komendy Linux</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </figure>
    
    <figure class="col-lg-3 col-md-6 col-sm-12 mx-auto">
        <div class="col-lg-12 col-md-12 col-sm-12 nav-to px-0">
            <a href="#" target="_blank">
                <img src="img/winserv.jpg" alt="windows server" class="image"/>
                <div class="cover my-auto py-auto">
                    <div class="cover-child">
                        <div>
                            <i class="fas fa-paper-plane fa-2x "></i>
                        </div>
                        <div>
                            <span>Windows Server<br/>&<br/>Linux</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </figure>
</div>
