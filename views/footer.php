 <footer class="container-fluid pb-5">

            <div class="row my-5 py-5">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 border-right border-light text-right">
                        <h4 class="px-1">Autorzy <i class="fab fa-facebook"></i></h4>

                        <div class="py-1 my-auto">
                            <div class="mx-1 display-inline position-relative">
                                <div class="mx-auto nav-to">
                                    <a href="https://www.facebook.com/JurasikAlbert" target="_blank">
                                        <img src="img/a1.jpg" class="autor-img" alt="1st autor"/>
                                        <div class="cover bg-primary">
                                            <div class="cover-child">

                                                <i class="fab fa-facebook-f"></i>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="mx-1 display-inline position-relative">
                                <div class="mx-auto nav-to">
                                    <a href="https://www.facebook.com/profile.php?id=100004764635683" target="_blank">
                                        <img src="img/a2.jpg" class="autor-img" alt="1st autor"/>
                                        <div class="cover bg-primary">
                                            <div class="cover-child">

                                                <i class="fab fa-facebook-f"></i>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="mx-1 display-inline position-relative">
                                <div class="mx-auto nav-to">
                                    <a href="https://www.facebook.com/mateusz.sobol.35" target="_blank">
                                        <img src="img/a3.jpg" class="autor-img" alt="1st autor"/>
                                        <div class="cover bg-primary">
                                            <div class="cover-child">

                                                <i class="fab fa-facebook-f"></i>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="py-1 my-auto">
                            <div class="mx-1 display-inline position-relative">
                                <div class="mx-auto nav-to">
                                    <a href="https://www.facebook.com/eryk.madera.18" target="_blank">
                                        <img src="img/a4.jpg" class="autor-img" alt="1st autor"/>
                                        <div class="cover bg-primary">
                                            <div class="cover-child">

                                                <i class="fab fa-facebook-f"></i>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="mx-1 display-inline position-relative">
                                <div class="mx-auto nav-to">
                                    <a href="https://www.facebook.com/daniel.wrublewski" target="_blank">
                                        <img src="img/a5.jpg" class="autor-img" alt="1st autor"/>
                                        <div class="cover bg-primary">
                                            <div class="cover-child">

                                                <i class="fab fa-facebook-f"></i>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="mx-1 display-inline position-relative">
                                <div class="mx-auto nav-to">
                                    <a href="https://www.facebook.com/maciek.zdrojewski.1" target="_blank">
                                        <img src="img/a6.jpg" class="autor-img" alt="1st autor"/>
                                        <div class="cover bg-primary">
                                            <div class="cover-child">

                                                <i class="fab fa-facebook-f"></i>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="py-1 my-auto">
                            <div class="mx-1 display-inline position-relative">
                                <div class="mx-auto nav-to">
                                    <a href="https://www.facebook.com/Arch255" target="_blank">
                                        <img src="img/a7.jpg" class="autor-img" alt="1st autor"/>
                                        <div class="cover bg-primary">
                                            <div class="cover-child">

                                                <i class="fab fa-facebook-f"></i>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="mx-1 display-inline position-relative">
                                <div class="mx-auto nav-to">
                                    <a href="https://www.facebook.com/filip.buganik.5?pageid=555844251127084&ftentidentifier=2183851981659628&padding=0" target="_blank">
                                        <img src="img/a8.jpg" class="autor-img" alt="1st autor"/>
                                        <div class="cover bg-primary">
                                            <div class="cover-child">

                                                <i class="fab fa-facebook-f"></i>

                                            </div>
                                        </div>
                                    </a>
                                </div>

                            </div>
                            <div class="mx-1 display-inline position-relative">
                                <div class="mx-auto nav-to">
                                    <a href="https://pl-pl.facebook.com/public/Micha%C5%82-Dzimira" target="_blank">
                                        <img src="img/a9.jpg" class="autor-img" alt="1st autor"/>
                                        <div class="cover bg-primary">
                                            <div class="cover-child">

                                                <i class="fab fa-facebook-f"></i>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="send" class="col-lg-6 col-md-6 col-sm-12 pr-5 float-right">

                    <form action="controllers/send.php" method="POST" action="controllers/send.php">
                        <div class="form-group">
                            <label for="name">Podaj e-mail:</label>
                            <input type="email" class="form-control" id="name" name="name" placeholder="Twój email">
                            <small id="nameHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <textarea rows="5" type="text" class="form-control" id="text" name="text" placeholder="wiadomość"></textarea>
                            <small id="nameHelp" class="form-text text-muted"></small>
                            <button type="submit" class="btn btn-md text-light my-2 px-5 btn-analogic-c">Wyślij</button>
                        </div>

                    </form>

                </div>
            </div>

        </footer>